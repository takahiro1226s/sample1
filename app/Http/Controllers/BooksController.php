<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book; //Book モデルを使えるようにする
use Validator; //バリデーションを使えるようにする
use Auth; //認証モデルを使用する(他の use を記述している箇所下に記述) ...

class BooksController extends Controller
{
    //コンストラクタ(このクラスが呼ばれたら最初に処理をする箇所) 
    public function __construct(){
        $this->middleware('auth');
    }
    
    //更新
    public function update(Request $request) {
        //バリデーション
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'item_name' => 'required|min:3|max:255',
            'item_number' => 'required|min:1|max:3',
            'item_amount' => 'required|max:6',
            'published' => 'required',
        ]);
        //バリデーション:エラー
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }
        //データ更新
        $books = Book::find($request->id);
        $books->item_name   = $request->item_name;
        $books->item_number = $request->item_number;
        $books->item_amount = $request->item_amount;
        $books->published   = $request->published;
        $books->save();
        return redirect('/');
    }
    
    public function store(Request $request){
        //バリデーション
        $validator = Validator::make($request->all(), [
            'item_name' => 'required|min:3|max:255',
            'item_number' => 'required|min:1|max:3',
            'item_amount' => 'required|max:6',
            'published' => 'required|date',
        ]);
        //バリデーション:エラー 
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }
        // Eloquent モデル
        $books = new Book;
        $books->item_name = $request->item_name; 
        $books->item_number = $request->item_number; 
        $books->item_amount = $request->item_amount; 
        $books->published = $request->published; 
        $books->save(); 
        //「/」ルートにリダイレクト 
        return redirect('/');
    }
    
    public function delete (Book $books){
        $book->delete();
        return redirect('/');
    }
    
    public function edit(Book $books) {
            //「/」ルートにリダイレクト 
        return view('booksedit', ['book' => $books]);
    }
    
    public function index() {
        $books = Book::orderBy('created_at', 'asc')->paginate(3);
        //使用例 1
        $auth = Auth::user()->id; //$auth に「id 」値のみ代入
        //使用例 2
        $auths = Auth::user(); //$auths に「id,name,email...」の複数値を代入
        //テンプレート側で「 $auths->プロパティ名 」で値取得が可能
        return view( 'books', [ 'books'=>$books, 'auths'=>$auths ] );
    }
}
