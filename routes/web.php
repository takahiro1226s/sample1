<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Book;
use Illuminate\Http\Request;

/**
* 本のダッシュボード表示 */
Route::get('/', 'BooksController@index');
/**
* 新「本」を追加 */
Route::post('/books', 'BooksController@store');

/**
* 本を削除 */
Route::post('/book/{book}', 'BooksController@delete');

// 本を更新
Route::post('/booksedit/{books}', 'BooksController@edit');

Route::post('/books/update', 'BooksController@update');



Auth::routes();

Route::get('/home', 'BooksController@index')->name('home');


