<!-- resources/views/books.blade.php -->
@extends('layouts.app')
@section('content')
    <div class="panel-body">
        <!-- バリデーションエラーの表示に使用-->
        @include('common.errors')
        <!-- バリデーションエラーの表示に使用-->
        
        <!-- 本登録フォーム -->
        <form action="{{ url('books/update') }}" method="POST" class="form-horizontal">
            <!-- 本のタイトル -->
            <div class="form-group">
                <label for="book" class="col-sm-3 control-label">Book</label>
                <div class="col-sm-6">
                    <input type="text" name="item_name" id="book-name" class="form-control" value="{{ $book->item_name }}">
                </div>
            </div>
            <div class="form-group">
                <label for="book" class="col-sm-3 control-label">Number</label>
                <div class="col-sm-6">
                    <input type="text" name="item_number" id="book-number" class="form-control" value="{{ $book->item_number }}">
                </div>
            </div>
            <div class="form-group">
                <label for="book" class="col-sm-3 control-label">Amount</label>
                <div class="col-sm-6">
                    <input type="text" name="item_amount" id="book-amount" class="form-control" value="{{ $book->item_amount }}">
                </div>
            </div>
                <div class="form-group">
                    <label for="book" class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-6">
                        <input type="text" name="published" id="published" class="form-control" value="{{ $book->published }}">
                </div>
            </div>
            <!-- id 値を送信 -->
            <input type="hidden" name="id" value="{{$book->id}}" /> <!--/ id 値を送信 -->
            <!-- 本 登録ボタン -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="glyphicon glyphicon-plus"></i> Save
                    </button>
                </div>
            </div>
            {{ csrf_field() }}
        </form>
    </div>
</div>
<!-- Book: 既に登録されてる本のリスト -->
@endsection
